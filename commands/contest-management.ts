import {SlashCommandBuilder} from '@discordjs/builders';
import {
    ActionRowBuilder,
    ChatInputCommandInteraction, EmbedBuilder,
    ModalActionRowComponentBuilder, ModalBuilder, PermissionFlagsBits,
    TextInputBuilder, TextInputStyle
} from "discord.js";
import prisma from "../client";
import {ConsoleColors, EmbedColors} from "../classes/enums";

module.exports = {
    data: new SlashCommandBuilder()
        .setName('contest-management')
        .setDescription("manages contests")
        .setDMPermission(false)
        .setDefaultMemberPermissions(PermissionFlagsBits.ManageGuild)
        .addSubcommand(subcommand =>
            subcommand
                .setName('create')
                .setDescription("creates a new contest"))
        .addSubcommand(subcommand =>
            subcommand
                .setName('list')
                .setDescription("lists current contests"))
        .addSubcommand(subcommand =>
            subcommand
                .setName('end')
                .setDescription("ends an ongoing contest")
                .addStringOption(option =>
                    option
                        .setName('id')
                        .setDescription("the id of the contest")
                        .setRequired(true))),
    async execute(interaction: ChatInputCommandInteraction) {
        if (interaction.options.getSubcommand() === 'create') {
            const modal = new ModalBuilder()
                .setTitle("Create a new contest")
                .setCustomId("5000+0301")
                .addComponents(
                    new ActionRowBuilder<ModalActionRowComponentBuilder>()
                        .addComponents(
                            new TextInputBuilder()
                                .setCustomId("name")
                                .setLabel("Name of the contest")
                                .setStyle(TextInputStyle.Short)
                                .setRequired(true)
                                .setMinLength(3)
                                .setMaxLength(100)
                                .setPlaceholder("XYZ contest")
                        ),
                    new ActionRowBuilder<ModalActionRowComponentBuilder>()
                        .addComponents(
                            new TextInputBuilder()
                                .setCustomId("description")
                                .setLabel("Description of the contest")
                                .setStyle(TextInputStyle.Paragraph)
                                .setRequired(true)
                                .setMinLength(1)
                                .setMaxLength(2000)
                        ),
                    new ActionRowBuilder<ModalActionRowComponentBuilder>()
                        .addComponents(
                            new TextInputBuilder()
                                .setCustomId("submissionsChannel")
                                .setLabel("ID of the submissions channel")
                                .setStyle(TextInputStyle.Short)
                                .setRequired(true)
                                .setMinLength(5)
                                .setMaxLength(30)
                        )
                )
            try {
                await interaction.showModal(modal)
            }
            catch (e) {
                console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, e)
                return
            }
        }
        else if (interaction.options.getSubcommand() === 'end') {
            const id = interaction.options.getString("id") as string

            const {name} = await prisma.contest.delete({
                where: {
                    id: Number.parseInt(id)
                },
                select: {
                    name: true,
                }})
            await interaction.reply(`Ended ${name}`)
        }
        else if (interaction.options.getSubcommand() === 'list') {
            const contests = await prisma.contest.findMany()
            const embed = new EmbedBuilder()
                .setTitle("Current contests")
                .setColor(EmbedColors.PRIMARY)
            for (let i = 0; i < contests.length; i++)
                embed.addFields([{name: contests[0].name, value: contests[0].id.toString()}])
            await interaction.reply({embeds: [embed], ephemeral: true})
        }
    },
};