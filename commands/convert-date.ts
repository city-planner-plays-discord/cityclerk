import {SlashCommandBuilder} from '@discordjs/builders';
import {ChatInputCommandInteraction} from "discord.js";

module.exports = {
    data: new SlashCommandBuilder()
        .setName('convert-date')
        .setDescription("let's you convert a date time to a Unix string")
        .setDMPermission(false)
        .addIntegerOption(option =>
            option
                .setName("year")
                .setDescription("Defaults to current UTC year")
                .setMinValue(1970))
        .addIntegerOption(option =>
            option
                .setName("month")
                .setDescription("Defaults to current UTC month")
                .setMinValue(1)
                .setMaxValue(12))
        .addIntegerOption(option =>
            option
                .setName("day")
                .setDescription("Defaults to current UTC day")
                .setMinValue(1)
                .setMaxValue(31))
        .addIntegerOption(option =>
            option
                .setName("hours")
                .setDescription("(24h format) defaults to current UTC hour")
                .setMinValue(0)
                .setMaxValue(23))
        .addIntegerOption(option =>
            option
                .setName("minutes")
                .setDescription("Defaults to current UTC minute")
                .setMinValue(0)
                .setMaxValue(60))
        .addIntegerOption(option =>
            option
                .setName("offset")
                .setDescription("Defaults to 0")
                .setMinValue(-12)
                .setMaxValue(12))
        .addStringOption(option =>
            option
                .setName('output')
                .setDescription("defaults to unix")
                .setChoices(
                    {name: "Plain number", value: "plain"},
                    {name: "Short time (e.g 9:41 PM)", value: ":t"},
                    {name: "Long Time (e.g. 9:41:30 PM)", value: ":T"},
                    {name: "Short Date (e.g. 30/06/2021)", value: ":d"},
                    {name: "Long Date (e.g. 30 June 2021)", value: ":D"},
                    {name: "Short Date/Time (e.g. 30 June 2021 9:41 PM)", value: ":f"},
                    {name: "Long Date/Time (e.g. Wednesday, June, 30, 2021 9:41 PM)", value: ":F"},
                    {name: "Relative Time (e.g. 2 months ago, in an hour)", value: ":R"},
                )),
    async execute(interaction: ChatInputCommandInteraction) {
        /* <t:TIMESTAMP:FLAG>
        * t: Short time (e.g 9:41 PM)
        * T: Long Time (e.g. 9:41:30 PM)
        * d: Short Date (e.g. 30/06/2021)
        * D: Long Date (e.g. 30 June 2021)
        * f (default): Short Date/Time (e.g. 30 June 2021 9:41 PM)
        * F: Long Date/Time (e.g. Wednesday, June, 30, 2021 9:41 PM)
        * R: Relative Time (e.g. 2 months ago, in an hour)
        * */
        const today = new Date()
        const flag = interaction.options.getString('output') ?? ":f"
        const offset = interaction.options.getInteger('offset') ?? 0

        const year = (interaction.options.getInteger('year') ?? today.getUTCFullYear()).toString().padStart(4, "0")
        const month = (interaction.options.getInteger('month') ?? today.getUTCMonth() + 1).toString().padStart(2, "0")
        const day = (interaction.options.getInteger('day') ?? today.getUTCDate()).toString().padStart(2, "0")
        const hours = (interaction.options.getInteger('hours') ?? today.getUTCHours()).toString().padStart(2, "0")
        const minutes = (interaction.options.getInteger('minutes') ?? today.getUTCMinutes()).toString().padStart(2, "0")

        const unix = Date.parse(`${year}-${month}-${day}T${hours}:${minutes}${(offset<0 ? "" : "+")}${(offset).toString().padStart(2, "0")}:00`) / 1000
        await interaction.reply({content: (flag === "plain") ? `${unix}` : `<t:${unix}${flag}>`, ephemeral: true})
    },
};