import {
    ActionRowBuilder,
    ApplicationCommandType,
    ContextMenuCommandBuilder,
    MessageContextMenuCommandInteraction, ModalActionRowComponentBuilder,
    ModalBuilder,
    TextInputBuilder,
    TextInputStyle
} from "discord.js";

module.exports = {
    data: new ContextMenuCommandBuilder()
        .setName('help-thread')
        .setType(ApplicationCommandType.Message)
        .setDMPermission(false),
    async execute(context: MessageContextMenuCommandInteraction) {
        const modal = new ModalBuilder()
            .setTitle("New help thread")
            .setCustomId(`5000+0400+${context.targetMessage.id}`)
            .setComponents(
                new ActionRowBuilder<ModalActionRowComponentBuilder>()
                .addComponents(
                    new TextInputBuilder()
                        .setCustomId("title")
                        .setLabel("title")
                        .setMaxLength(100)
                        .setStyle(TextInputStyle.Short)
                        .setPlaceholder("title of the help thread")
                ))

        await context.showModal(modal)
    },
};