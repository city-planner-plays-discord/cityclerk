import {SlashCommandBuilder} from '@discordjs/builders';
import {ActionRowBuilder, ButtonBuilder, ButtonStyle, ChatInputCommandInteraction, EmbedBuilder} from "discord.js";
import prisma from "../client";
import {ConsoleColors} from "../classes/enums";

module.exports = {
    data: new SlashCommandBuilder()
        .setName('contest')
        .setDescription("Make a submission for a current contest")
        .setDMPermission(false),
    async execute(interaction: ChatInputCommandInteraction) {
        try {
            const contests = await prisma.contest.findMany()

            if (contests.length > 0) {
                const embed = new EmbedBuilder()
                    .setTitle("Current contests")
                const row = new ActionRowBuilder<ButtonBuilder>()
                for (let i = 0; i < contests.length; i++) {
                    embed.addFields([{name: `${i + 1}: ${contests[i].name}`, value: contests[i].description}])
                    row.addComponents([
                        new ButtonBuilder()
                            .setCustomId(`4000+0201+${contests[i].submissionsChannelID}+${contests[i].id}`)
                            .setStyle(ButtonStyle.Secondary)
                            .setLabel((i+1).toString())
                    ])
                }

                await interaction.reply({ephemeral: true, components: [row], embeds: [embed]})
            }
            else await interaction.reply({ephemeral: true, content: "There are currently no ongoing contests."})
        }
        catch (e) {
            console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, e)
            return
        }
    },
};
