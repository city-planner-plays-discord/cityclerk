import {SlashCommandBuilder} from '@discordjs/builders';
import {
    ChannelType,
    ChatInputCommandInteraction,
    ForumChannel, PermissionFlagsBits,
    TextChannel
} from "discord.js";
import {Channels, ConsoleColors, Roles} from "../classes/enums";
import prisma from "../client";

module.exports = {
    data: new SlashCommandBuilder()
        .setName('series')
        .setDescription("makes the bot say something")
        .setDMPermission(false)
        .setDefaultMemberPermissions(PermissionFlagsBits.ManageGuild)
        .addSubcommand(subcommand =>
            subcommand
                .setName('create')
                .setDescription("creates a new series")
                .addStringOption(option =>
                    option
                        .setName('name')
                        .setDescription("the name of the series. Capitalization matters!")
                        .setRequired(true))
                .addChannelOption(option =>
                    option
                        .setName('channel')
                        .setRequired(false)
                        .setDescription("channel of the series. If none is provided one will be created")
                        .addChannelTypes(ChannelType.GuildText, ChannelType.GuildForum))
                .addStringOption(option =>
                    option
                        .setName('start-date')
                        .setDescription("the date the series started on. YYYY-MM-DD. Defaults to now")
                        .setRequired(false)))
        .addSubcommand(subcommand =>
            subcommand
                .setName('end')
                .setDescription("ends a series")
                .addStringOption(option =>
                    option
                        .setName('name')
                        .setDescription("the name of the series. Capitalization matters!")
                        .setRequired(true))
                .addBooleanOption(option =>
                    option
                        .setName('archive')
                        .setDescription("Whether to archive the associated channel")
                        .setRequired(true))
                .addStringOption(option =>
                    option
                        .setName('end-date')
                        .setDescription("the date the series ended on. YYYY-MM-DD. Defaults to now")
                        .setRequired(false))),
    async execute(interaction: ChatInputCommandInteraction) {
        const seriesName = interaction.options.getString('name') as string
        if (interaction.options.getSubcommand() === 'create') {
            try {
                const seriesChannel = interaction.options.getChannel('channel') as (TextChannel | ForumChannel) ?? await interaction.guild?.channels.create({
                    name: seriesName,
                    type: ChannelType.GuildForum,
                    parent: Channels.TALK_CATEGORY
                })
                const startDate = new Date(interaction.options.getString('start-date') ?? Date.now())

                const dbCreate = prisma.series.create({
                    data: {
                        seriesID: seriesName.toLowerCase().replaceAll(' ', '-'),
                        seriesName: seriesName,
                        seriesChannelID: seriesChannel.id,
                        startDate: startDate
                    }
                })

                await Promise.all([dbCreate, interaction.reply(`${seriesName} created successfully!`)])
            }
            catch (e) {
                console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, e)
            }
        }
        else if (interaction.options.getSubcommand() === 'end') {
            const archived = interaction.options.getBoolean('archive') ?? true
            const endDate = new Date(interaction.options.getString('start-date') ?? Date.now())
            try {
                const {seriesChannelID} = await prisma.series.update({where: {seriesName: seriesName}, data: {archived: archived, endDate: endDate}, select: {seriesChannelID: true}})
                if (archived && interaction.guild != null) {
                    const channel = await interaction.guild.channels.fetch(seriesChannelID) as (TextChannel | ForumChannel)
                    // archive channel
                    await Promise.all([channel.permissionOverwrites.set([{id: Roles.EVERYONE, deny: [PermissionFlagsBits.SendMessages, PermissionFlagsBits.AddReactions, PermissionFlagsBits.SendMessagesInThreads, PermissionFlagsBits.CreatePublicThreads, PermissionFlagsBits.CreatePrivateThreads]}]), channel.setParent(Channels.ARCHIVE), interaction.reply(`${seriesName} created successfully!`)])
                }
            }
            catch (e) {
                console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, e)
            }
        }
    },
};