import {SlashCommandBuilder} from '@discordjs/builders';
import {
    ChannelType,
    ChatInputCommandInteraction, EmbedBuilder,
    PermissionFlagsBits,
    TextChannel
} from "discord.js";
import {ConsoleColors, EmbedColors} from "../classes/enums";
import prisma from "../client";

module.exports = {
    data: new SlashCommandBuilder()
        .setName('image-only')
        .setDescription("manages image only channels")
        .setDMPermission(false)
        .setDefaultMemberPermissions(PermissionFlagsBits.ManageGuild)
        .addSubcommand(subcommand =>
            subcommand
                .setName('add')
                .setDescription("makes a channel image only")
                .addChannelOption(option =>
                    option
                        .setName('channel')
                        .setRequired(true)
                        .setDescription("channel to make image only")
                        .addChannelTypes(ChannelType.GuildText))
                .addStringOption(option =>
                    option
                        .setName('message')
                        .setDescription("message users will receive when their message doesn't contain an image")
                        .setRequired(false)))
        .addSubcommand(subcommand =>
            subcommand
                .setName('remove')
                .setDescription("removes a channel from image only channels")
                .addChannelOption(option =>
                    option
                        .setName('channel')
                        .setRequired(true)
                        .setDescription("channel to remove from image only channels")
                        .addChannelTypes(ChannelType.GuildText))),
    async execute(interaction: ChatInputCommandInteraction) {
        const channel = interaction.options.getChannel('channel') as TextChannel
        if (interaction.options.getSubcommand() === 'add') {
            try {
                const embed = new EmbedBuilder()
                    .setTitle("This channel is an image-only channel now.")
                    .setDescription("Messages that do not contain images will be removed.")
                    .setColor(EmbedColors.WARNING)
                const prismaCreate = prisma.imageOnlyChannel.create({
                    data: {
                        channelID: channel.id,
                        customMessage: interaction.options.getString('message') ?? undefined
                    }
                })
                await Promise.all([channel.send({embeds: [embed]}), prismaCreate, interaction.reply({content: `Made <#${channel.id}> an image-only channel.`, ephemeral: true})])
            }
            catch (e) {
                console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, e)
            }
        }
        else if (interaction.options.getSubcommand() === 'remove') {
            try {
                const prismaDrop = prisma.imageOnlyChannel.delete({where: {channelID: channel.id}})
                await Promise.all([prismaDrop, interaction.reply({content: `Removed <#${channel.id}> from image-only channels.`, ephemeral: true})])
            }
            catch (e) {
                console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, e)
            }
        }
    },
};