import {SlashCommandBuilder} from '@discordjs/builders';
import {
    ChannelType,
    ChatInputCommandInteraction, PermissionFlagsBits,
    TextChannel
} from "discord.js";
import {ConsoleColors} from "../classes/enums";

module.exports = {
    data: new SlashCommandBuilder()
        .setName('say')
        .setDescription("makes the bot say something")
        .setDefaultMemberPermissions(PermissionFlagsBits.ManageMessages)
        .setDMPermission(false)
        .addSubcommand(subcommand =>
            subcommand
                .setName('channel')
                .setDescription("makes the bot say something in a channel")
                .addChannelOption(option =>
                    option
                        .setName("channel")
                        .setRequired(true)
                        .addChannelTypes(ChannelType.GuildText, ChannelType.PrivateThread, ChannelType.PublicThread)
                        .setDescription("the channel to post the message in"))
                .addStringOption(option =>
                    option
                        .setName('message')
                        .setRequired(true)
                        .setMaxLength(2000)
                        .setDescription("the message")))
        .addSubcommand(subcommand =>
            subcommand
                .setName('dms')
                .setDescription("makes the bot say something in a user's DMs")
                .addUserOption(option =>
                    option
                        .setName('user')
                        .setRequired(true)
                        .setDescription("the user to dm"))
                .addStringOption(option =>
                    option
                        .setName('message')
                        .setRequired(true)
                        .setMaxLength(2000)
                        .setDescription("the message"))),
    async execute(interaction: ChatInputCommandInteraction) {
        if (interaction.options.getSubcommand() === 'channel') {
            let channel = interaction.options.getChannel('channel')
            if (channel != null && channel.type == ChannelType.GuildText) {
                channel = await interaction.client.channels.fetch(channel.id) as TextChannel
                try {
                    await Promise.all([channel.send(interaction.options.getString('message') as string), interaction.reply({content: `Sent message in <#${channel.id}>`, ephemeral: true})])
                }
                catch (e) {
                    console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, e)
                }
            }
        }
        else if (interaction.options.getSubcommand() === 'dms') {
            let user = interaction.options.getUser('user')
            if (user != null) {
                try {
                    await user.createDM()
                    await Promise.all([user.send(interaction.options.getString('message') as string), interaction.reply({content: `Sent message to <@${user.id}>`, ephemeral: true})])
                }
                catch (e: any) {
                    if ("code" in e && e.code == 50007) console.warn(`${ConsoleColors.FGRED}[API ERROR]${ConsoleColors.RESET}`, e) //if dms of user closed or bot blocked warn
                    console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, e)
                }
            }
        }
        else console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, "unknown subcommand")
    },
};