import {ActionRowBuilder, Embed, ModalBuilder, TextInputBuilder, TextInputStyle} from "discord.js";
import prisma from "../client";
import {hashCode} from "./String";

export class Contest {
    public readonly name: string
    public readonly description: string
    private readonly _submissionsChannel: string | null = null
    public readonly channels: string[] = []
    public readonly fields: TextInputBuilder[] = []
    constructor({fields, title, description}: Embed) {
        if (title == null || description == null || fields.length === 0) throw Error("Embed data incomplete")
        this.name = title
        this.description = description

        let m = new Map<string, string[]>()
        while (fields.length > 0) {
            const f = fields.pop()
            if (typeof f == "undefined") return

            if(f.name === "submissionsChannel") this._submissionsChannel =  f.value.trim()
            else {
                m.set(f.name, f.value.replace(/\s*,\s*/g, ",").split(','))
            }
        }

        new Map([...m.entries()].sort())
            .forEach((v, k) => {
                const input = new TextInputBuilder()
                    .setCustomId(k)
                    .setLabel(v[0])
                    .setPlaceholder(v[1])
                    .setStyle(TextInputStyle[v[2].charAt(0).toUpperCase() + v[2].slice(1) as "Paragraph" | "Short"])
                if (v[3] !== undefined && v[3] !== "") input.setMinLength(Number.parseInt(v[3]))
                if (v[4] !== undefined && v[4] !== "") input.setMaxLength(Number.parseInt(v[4]))
                this.fields.push(input)
            })
    }
    get submissionsChannel(): string {
        if (this._submissionsChannel === null) throw Error("No submissions channel")
        return this._submissionsChannel;
    }

    fieldsToJson() {
        const arr = []
        for (let i = 0; i < this.fields.length; i++) {
            arr.push(this.fields[i].data)
        }
        return JSON.stringify(arr)
    }

    create() {
        let modal = new ModalBuilder()
            .setTitle(this.name)
            .setCustomId("0000")
        for (let i = 0; i < this.fields.length; i++)
            modal.addComponents(new ActionRowBuilder<TextInputBuilder>().addComponents(this.fields[i]))
        return prisma.contest.create({
            data: {
                name: this.name,
                id: hashCode(this.name + this.submissionsChannel),
                modal: JSON.stringify(modal.toJSON()),
                description: this.description,
                submissionsChannelID: this.submissionsChannel
            }
        })
    }
}