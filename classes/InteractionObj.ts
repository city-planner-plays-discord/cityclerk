export class InteractionObj {
    public readonly id: number;
    public readonly interactionType: number;
    public readonly usage: number;
    public readonly reference: string | undefined;
    public readonly otherReference: string | undefined;

    constructor(customId: string) {
        let x: string, u: string
        [x, u, this.reference, this.otherReference] =  customId.split('+')
        this.id = Number.parseInt(x + u)
        this.interactionType = Number.parseInt(x)
        this.usage = Number.parseInt(u)
    }
}
/*
* xaaa+zzbb+c...+[d...]
* x: interaction type, aaa: interaction subtype (
*       0: RESERVED
*       1: slash command,
*       2: context menu command
*           [000: RESERVED, 001: message, 002: user],
*       3: any select menu
*           [000: RESERVED, 001: string, 002: user, 003: mentionable, 004: role, 005: channel],
*       4: button
*       5: modal submit
*       6: autocomplete
*       9: RESERVED
* )
* zz: group, bb: usage (
*       00: RESERVED
*       01: roles
*           [00: notifications <button>]
*       02: contest submission
*           [00: RESERVED, 01: create submission <button>, 02: create submission <modal>]
*       03: contests
*           [00: RESERVED, 01: create contest <modal>, 02: edit fields <button>, 03: edit fields <modal>, 04: show modal <button>, 05: create contest <button>]
*       04: help thread
* )
* */