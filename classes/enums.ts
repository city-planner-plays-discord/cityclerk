export const enum ConsoleColors {
    RESET = "\x1b[0m",
    BRIGHT = "\x1b[1m",
    DIM = "\x1b[2m",
    UNDERSCORE = "\x1b[4m",
    BLINK = "\x1b[5m",
    REVERSE = "\x1b[7m",
    HIDDEN = "\x1b[8m",

    FGBLACK = "\x1b[30m",
    FGRED = "\x1b[31m",
    FGGREEN = "\x1b[32m",
    FGYELLOW = "\x1b[33m",
    FGBLUE = "\x1b[34m",
    FGMAGENTA = "\x1b[35m",
    FGCYAN = "\x1b[36m",
    FGWHITE = "\x1b[37m",

    BGBLACK = "\x1b[40m",
    BGRED = "\x1b[41m",
    BGGREEN = "\x1b[42m",
    BGYELLOW = "\x1b[43m",
    BGBLUE = "\x1b[44m",
    BGMAGENTA = "\x1b[45m",
    BGCYAN = "\x1b[46m",
    BGWHITE = "\x1b[47m",
}

export const enum EmbedColors {
    PRIMARY = "#192651",
    SECONDARY = "#fdffff",
    DANGER = "#da1200",
    WARNING = "#f69e01",
    INFO = "#008de5",
    OK = "#0f9d11"
}

export const enum Roles {
    DEALS = "789213974381461545",
    GEOTRIVIA = "875385193375076352",
    YOUTUBE = "877795529982111756",
    BAR = "938257530453364748",
    EVERYONE = "774118086222675999"
}

export const enum Channels {
    MODERATION_LOG = "858623260471066634",
    USER_LOG = "789116705305002015",
    TALK_CATEGORY = "785225951721685012",
    ARCHIVE = "851390684774400030",
    HELP_CHANNEL = "968273009339170836"
}