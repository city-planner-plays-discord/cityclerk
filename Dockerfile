# syntax=docker.io/docker/dockerfile:1
FROM node:16.18.1

# create dir
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
WORKDIR /home/node/app

# build dependencies
COPY ./ ./
USER node
RUN npm install
RUN ls -a
RUN npm run build

# copy in source code
# COPY --chown=node:node ./ ./

# start bot
CMD npx prisma migrate deploy && node -r dotenv/config deploy-commands.js && npm run start
