import {
    EmbedBuilder,
    ModalBuilder,
    ModalSubmitInteraction,
    TextChannel,
} from "discord.js";
import prisma from "../client";
import {ConsoleColors, EmbedColors} from "../classes/enums";
import {InteractionObj} from "../classes/InteractionObj";
import {hashCode} from "../classes/String";

export async function contestModalSubmit(interaction: ModalSubmitInteraction, interactionObj: InteractionObj) {
    if (interactionObj.reference == null || interactionObj.otherReference == null) return
    try {
        let fields: string[] = []
        let i = 1
        while (i < 6) {
            try {
                fields[i-1] = interaction.fields.getTextInputValue(i.toString())
            }
            catch (e) {
                break
            }
            i++
        }

        const channel = interaction.client.channels.fetch(interactionObj.reference)
        const prismaCreate = prisma.contestSubmissions.create({
            data: {
                id: hashCode(interaction.user.id, Number.parseInt(interactionObj.otherReference)),
                memberID: interaction.user.id,
                submission: fields,
                contest: {
                    connect: {id: Number.parseInt(interactionObj.otherReference)}
                }
            },
            include: {
                contest: {
                    select: {
                        modal: true
                    }
                }
            }
        })

        const [submissionsChannel, {contest: {modal: m}}] = await Promise.all([channel, prismaCreate]) as [TextChannel, {contest: {modal: object}}]
        const {components} = new ModalBuilder(JSON.parse(m.toString()))

        const responseEmbed = new EmbedBuilder()
            .setTitle("Your contest submission")
            .setColor(EmbedColors.SECONDARY)

        const submissionEmbed = new EmbedBuilder()
            .setColor(EmbedColors.PRIMARY)
            .setAuthor({name: interaction.user.discriminator,  iconURL: interaction.user.displayAvatarURL()})
            .setFields([
                {name: "User", value: `<@${interaction.user.id}>`},
            ])

        for (let i = 0; i < components.length; i++) {
            const name = components[i].components[0].data.label
            if (name == undefined) return
            responseEmbed.addFields([
                {name: name, value: fields[i]}
            ])
            submissionEmbed.addFields([
                {name: name, value: fields[i]}
            ])
        }

        await Promise.all([interaction.reply({embeds: [responseEmbed], ephemeral: true}), submissionsChannel.send({embeds: [submissionEmbed]})])
    }
    catch (e: any) {
            if (Object.hasOwn(e, "code") && e.code == "P2002") {
            await interaction.reply({content: "Looks like you have already submitted something.", ephemeral: true})
            return
        }
        console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, e)
        return
    }
}