import {
    ChannelType,
    DiscordAPIError,
    ForumChannel,
    Message,
    ModalSubmitInteraction
} from "discord.js";
import {Channels, ConsoleColors} from "../classes/enums";

export async function helpThreadModalSubmit(interaction: ModalSubmitInteraction, messageId: string | undefined) {

    if (messageId == null || interaction.channel == null || interaction.channel.type !== ChannelType.GuildText) return console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, "messageId or interaction.channel null!")

    try {
        const [helpChannel, message] = await Promise.all([interaction.client.channels.fetch(Channels.HELP_CHANNEL), interaction.channel.messages.fetch(messageId)]) as [ForumChannel | null, Message]

        if (helpChannel == null) return console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, "helpChannel null!")

        const title = interaction.fields.getTextInputValue("title")
        const files = (message.attachments.first() === undefined) ? undefined : Array.from(message.attachments.values())
        const thread = await helpChannel.threads.create({
            name: title,
            message: {
                content: `<@${message.author.id}> - ${message.content}`,
                files: files
            }
        })
        await Promise.all([message.reply({content: `There was a help thread created for you in <#${thread.id}>`}), interaction.reply({content: "Successfully created thread", ephemeral: true})])
    }
    catch (e) {
        if (e instanceof DiscordAPIError) return console.trace(`${ConsoleColors.FGRED}[API ERROR]${ConsoleColors.RESET}`, e)
        return console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, e)
    }
}