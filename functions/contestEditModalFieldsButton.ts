import {
    ActionRowBuilder,
    ButtonInteraction,
    DiscordAPIError,
    ModalActionRowComponentBuilder,
    ModalBuilder,
    TextInputBuilder,
    TextInputStyle
} from "discord.js";
import {ConsoleColors} from "../classes/enums";

export async function contestEditModalFieldsButton(interaction: ButtonInteraction) {
    const modal = new ModalBuilder()
        .setCustomId(`0500+0303+${interaction.message.id}`)
        .setTitle("Contest modal fields")
        .addComponents(
            new ActionRowBuilder<ModalActionRowComponentBuilder>()
                .addComponents(
                    new TextInputBuilder()
                        .setCustomId("1")
                        .setLabel("Field #1")
                        .setStyle(TextInputStyle.Short)
                        .setPlaceholder("label,placeholder,type(paragraph|short),min?,max?")
                        .setRequired(true)),
            new ActionRowBuilder<ModalActionRowComponentBuilder>()
                .addComponents(
                    new TextInputBuilder()
                        .setCustomId("2")
                        .setLabel("Field #2")
                        .setStyle(TextInputStyle.Short)
                        .setPlaceholder("label,placeholder,type(paragraph|short),min?,max?")
                        .setRequired(false)),
            new ActionRowBuilder<ModalActionRowComponentBuilder>()
                .addComponents(
                    new TextInputBuilder()
                        .setCustomId("3")
                        .setLabel("Field #3")
                        .setStyle(TextInputStyle.Short)
                        .setPlaceholder("label,placeholder,type(paragraph|short),min?,max?")
                        .setRequired(false)),
            new ActionRowBuilder<ModalActionRowComponentBuilder>()
                .addComponents(
                    new TextInputBuilder()
                        .setCustomId("4")
                        .setLabel("Field #4")
                        .setStyle(TextInputStyle.Short)
                        .setPlaceholder("label,placeholder,type(paragraph|short),min?,max?")
                        .setRequired(false)),
            new ActionRowBuilder<ModalActionRowComponentBuilder>()
                .addComponents(
                    new TextInputBuilder()
                        .setCustomId("5")
                        .setLabel("Field #5")
                        .setStyle(TextInputStyle.Short)
                        .setPlaceholder("label,placeholder,type(paragraph|short),min?,max?")
                        .setRequired(false)
                )
        )
    try {
        await interaction.showModal(modal)
    }
    catch (e) {
        if (e instanceof DiscordAPIError) return console.trace(`${ConsoleColors.FGRED}[API ERROR]${ConsoleColors.RESET}`, e)
        return console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, e)
    }
}