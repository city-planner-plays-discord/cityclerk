import {ActionRowBuilder, ButtonBuilder, ButtonStyle, EmbedBuilder, ModalSubmitInteraction} from "discord.js";
import {ConsoleColors, EmbedColors} from "../classes/enums";

export async function contestCreateModalSubmit(interaction: ModalSubmitInteraction) {
    try {
        const name = interaction.fields.getTextInputValue("name")
        const description = interaction.fields.getTextInputValue("description")
        const submissionsChannel = interaction.fields.getTextInputValue("submissionsChannel")

        const embed = new EmbedBuilder()
            .setTitle(name)
            .setColor(EmbedColors.PRIMARY)
            .setDescription(description)
            .setFields([{name: "submissionsChannel", value: submissionsChannel}])

        const actionRow = new ActionRowBuilder<ButtonBuilder>()
            .addComponents(
                new ButtonBuilder()
                    .setCustomId("4000+0302")
                    .setLabel("Edit Modal Fields")
                    .setStyle(ButtonStyle.Secondary),
                new ButtonBuilder()
                    .setCustomId("4000+0304")
                    .setLabel("Show Modal")
                    .setStyle(ButtonStyle.Secondary),
                new ButtonBuilder()
                    .setCustomId("4000+0305")
                    .setLabel("Create Contest")
                    .setStyle(ButtonStyle.Primary),
            )

        await interaction.reply({embeds: [embed], components: [actionRow], ephemeral: true})
    }
    catch (e) {
        console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, e)
        return
    }
}