import {
    ButtonInteraction, ModalBuilder,
} from "discord.js";
import {ConsoleColors} from "../classes/enums";
import {InteractionObj} from "../classes/InteractionObj";
import prisma from "../client";

export async function contestModalButton(interaction: ButtonInteraction, interactionObj: InteractionObj) {
    if (interactionObj.otherReference == undefined) return

    try {
        const contest = await prisma.contest.findUniqueOrThrow({
            where: {id: Number.parseInt(interactionObj.otherReference)}
        })
        if (contest.modal == null) return
        const modal = new ModalBuilder(JSON.parse(contest.modal.toString()))
            .setCustomId(`5000+0202+${interactionObj.reference}+${interactionObj.otherReference}`)
        await interaction.showModal(modal)
    }
    catch (e) {
        return console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, e)
    }
}