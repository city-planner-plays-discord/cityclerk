import {
    ActionRowBuilder,
    ButtonInteraction, DiscordAPIError,
    ModalActionRowComponentBuilder,
    ModalBuilder
} from "discord.js";
import {Contest} from "../classes/Contest";
import {ConsoleColors} from "../classes/enums";

export async function contestShowModalButton(interaction: ButtonInteraction) {
    try {
        const contest = new Contest(interaction.message.embeds[0])

        const modal = new ModalBuilder()
            .setCustomId("0000+0000")
            .setTitle(contest.name)

        for (let i = 0; i < contest.fields.length; i++)
            modal.addComponents(
                new ActionRowBuilder<ModalActionRowComponentBuilder>()
                    .addComponents(contest.fields[i]))

        await interaction.showModal(modal)
    }
    catch (e) {
        if (e instanceof DiscordAPIError) return console.trace(`${ConsoleColors.FGRED}[API ERROR]${ConsoleColors.RESET}`, e)
        return console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, e)
    }
}