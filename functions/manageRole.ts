import {GuildMember} from "discord.js";

/**
 * checks if member has role
 * @description false: adds role
 * @description true: removes role
 **/
export async function manageRole(roleId: string, member: GuildMember) {
    const role = member.roles.cache.find((role) => role.id === roleId)
    if (typeof role !== "undefined") {
        await member.roles.remove(role)
        return "Removed"
    } else {
        await member.roles.add(roleId)
        return "Added"
    }
}