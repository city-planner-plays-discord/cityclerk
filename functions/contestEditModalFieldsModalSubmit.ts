import {ChannelType, EmbedBuilder, ModalMessageModalSubmitInteraction} from "discord.js";

export async function contestEditModalFieldsModalSubmit(interaction: ModalMessageModalSubmitInteraction, messageId: string | undefined) {
    if (interaction.channel == null || messageId == null || interaction.channel.type !== ChannelType.GuildText) return
    const message = await interaction.channel.messages.fetch(messageId)
    const embed = new EmbedBuilder(message.embeds[0].data)

    for (let i = 1; i < 6; i++) {
        let a = interaction.fields.getTextInputValue(`${i}`)
        if (a !== undefined && a !== "" && a !== " ") embed.addFields([{name: `${i}`, value: a}])
    }

    await interaction.update({embeds: [embed]})
}