import * as fs from "fs";
import * as path from 'node:path'
import {Client, Collection, GatewayIntentBits, Partials} from 'discord.js';
import {CommandoClient} from "./types/discordapi";
import Airtable from 'airtable';
import {ConsoleColors} from "./classes/enums";

const client = new Client({
    intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildModeration, GatewayIntentBits.GuildMembers, GatewayIntentBits.MessageContent, GatewayIntentBits.GuildMessages],
    partials: [Partials.Channel, Partials.Message],
}) as CommandoClient;

client.commands = new Collection();
const commandsPath = path.join(__dirname, 'commands');
const commandFiles = fs.readdirSync(commandsPath).filter(file => file.endsWith('.js'));

const eventsPath = path.join(__dirname, 'events');
const eventFiles = fs.readdirSync(eventsPath).filter((file: string) => file.endsWith('.js'));

for (const file of commandFiles) {
    const filePath = path.join(commandsPath, file);
    const command = require(filePath);
    // Set a new item in the Collection with the key as the command name and the value as the exported module
    if ('data' in command && 'execute' in command) {
        client.commands.set(command.data.name, command);
    } else {
        console.warn(`${ConsoleColors.FGYELLOW}[WARNING]${ConsoleColors.RESET} The command at ${filePath} is missing a required "data" or "execute" property.`);
    }
}

for (const file of eventFiles) {
    const filePath = path.join(eventsPath, file);
    const event = require(filePath);
    if (event.once) {
        client.once(event.name, (...args: any) => event.execute(...args));
    } else {
        client.on(event.name, (...args: any) => event.execute(...args));
    }
}

Airtable.configure({
    apiKey: process.env.AIRTABLE_APIKEY,
    endpointUrl: process.env.AIRTABLE_ENDPOINT_URL
});
const base = Airtable.base(process.env.AIRTABLE_BASE);

client.login(process.env.TOKEN);
export {base, client}