-- CreateTable
CREATE TABLE "Series" (
    "seriesID" TEXT NOT NULL,
    "seriesName" TEXT NOT NULL,
    "seriesChannelID" TEXT NOT NULL,
    "startDate" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "endDate" TIMESTAMP(3),
    "archived" BOOLEAN NOT NULL DEFAULT false,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Series_pkey" PRIMARY KEY ("seriesID")
);

-- CreateTable
CREATE TABLE "ImageOnlyChannel" (
    "channelID" TEXT NOT NULL,
    "customMessage" TEXT NOT NULL DEFAULT 'You have to post an image with your message',
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "ImageOnlyChannel_pkey" PRIMARY KEY ("channelID")
);

-- CreateTable
CREATE TABLE "Contest" (
    "id" BIGINT NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "modal" JSONB NOT NULL,
    "submissionsChannelID" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "Contest_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "ContestSubmissions" (
    "id" BIGINT NOT NULL,
    "memberID" TEXT NOT NULL,
    "contestID" BIGINT NOT NULL,
    "submission" JSONB NOT NULL DEFAULT '{}',
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "ContestSubmissions_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Series_seriesName_key" ON "Series"("seriesName");

-- CreateIndex
CREATE INDEX "ImageOnlyChannel_channelID_idx" ON "ImageOnlyChannel" USING HASH ("channelID");

-- CreateIndex
CREATE INDEX "ContestSubmissions_memberID_idx" ON "ContestSubmissions" USING HASH ("memberID");

-- AddForeignKey
ALTER TABLE "ContestSubmissions" ADD CONSTRAINT "ContestSubmissions_contestID_fkey" FOREIGN KEY ("contestID") REFERENCES "Contest"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
