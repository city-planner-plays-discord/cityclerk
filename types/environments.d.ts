export {};

declare global {
    namespace NodeJS {
        interface ProcessEnv {
            DATABASE_URL: string;
            TOKEN: string;
            CLIENT_ID: string;
            GUILD_ID: string;
            CLIENT_SECRET: string;
            CLIENT_PUBLIC_KEY: string;
            AIRTABLE_BASE: string;
            AIRTABLE_ENDPOINT_URL: string;
            AIRTABLE_APIKEY: string;
        }
    }
}