import {Client, Collection} from "discord.js";

interface CommandoClient extends Client {
    commands: Collection<string, any>;
}

export {CommandoClient};
