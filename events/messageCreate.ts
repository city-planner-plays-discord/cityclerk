import {Events, Message, PermissionFlagsBits} from "discord.js";
import prisma from "../client";
import {ConsoleColors} from "../classes/enums";

module.exports = {
    name: Events.MessageCreate,
    async execute(message: Message) {
        //console.info("MESSAGE CREATE")
        if (message.author.bot) {return}
        if (message.channel.isTextBased() && !message.channel.isThread() && message.member !== null && message.channel.id !== null && !message.member.permissions.has(PermissionFlagsBits.ManageMessages, true)) {
            try {
                const imageOnlyChannel = await prisma.imageOnlyChannel.findUniqueOrThrow({where: {channelID: message.channel.id}})
                await Promise.all([message.delete(), message.member.send(imageOnlyChannel.customMessage)])
            }
            catch (e: any) {
                if ("code" in e && e.code == "P2025") return
                else return console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, e)
            }
        }
    },
};