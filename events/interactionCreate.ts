import {Events, GuildMember, Interaction, ModalMessageModalSubmitInteraction} from "discord.js";
import {InteractionObj} from "../classes/InteractionObj";
import {manageRole} from "../functions/manageRole";
import {ConsoleColors} from "../classes/enums";
import {contestModalSubmit} from "../functions/contestModalSubmit";
import {contestCreateModalSubmit} from "../functions/contestCreateModalSubmit";
import {contestEditModalFieldsButton} from "../functions/contestEditModalFieldsButton";
import {contestShowModalButton} from "../functions/contestShowModalButton";
import {contestEditModalFieldsModalSubmit} from "../functions/contestEditModalFieldsModalSubmit";
import {client} from "../main";
import {Contest} from "../classes/Contest";
import {helpThreadModalSubmit} from "../functions/helpThreadModalSubmit";
import {contestModalButton} from "../functions/contestModalButton";

module.exports = {
    name: Events.InteractionCreate,
    async execute(interaction: Interaction) {
        console.info("INTERACTION CREATE")
        try {
            if (interaction.isCommand()) {

                const command = client.commands.get(interaction.commandName);

                if (!command) return;

                try {
                    await command.execute(interaction);
                } catch (error) {
                    console.error(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, error);
                    await interaction.reply({
                        content: 'There was an error while executing this command!',
                        ephemeral: true
                    });
                }
            }
            /*
            * 4000 Button interaction
            * */
            else if (interaction.isButton()) {
                const interactionObj = new InteractionObj(interaction.customId)
                switch (interactionObj.usage) {
                    case 1_00: // notification role buttons
                        if (interactionObj.reference == undefined) return
                        const action = await manageRole(interactionObj.reference, interaction.member as GuildMember)
                        try {
                            await interaction.reply({
                                content: `${action} role ${interactionObj.otherReference}`,
                                ephemeral: true
                            })
                        } catch (e) {
                            console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, e)
                        }
                        break;
                    case 2_01: // new contest submission
                        await contestModalButton(interaction, interactionObj)
                        break;
                    case 3_02: // edit contest modal fields
                        await contestEditModalFieldsButton(interaction)
                        break;
                    case 3_04: // show contest modal
                        await contestShowModalButton(interaction)
                        break;
                    case 3_05: // create contest
                        try {
                            const contest = new Contest(interaction.message.embeds[0])
                            await contest.create()
                            await interaction.update({content: "Successfully created contest!"})
                        } catch (e) {
                            console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, e)
                            return
                        }
                        break;
                }
            }
            /*
            * 5000 Modal submit interaction
            * */
            else if (interaction.isModalSubmit()) {
                const interactionObj = new InteractionObj(interaction.customId)
                switch (interactionObj.usage) {
                    case 2_02: // contest submission
                        if (interactionObj.reference == undefined) return
                        await contestModalSubmit(interaction, interactionObj)
                        break;
                    case 3_01: // initial contest creation modal
                        await contestCreateModalSubmit(interaction)
                        break;
                    case 3_03: // contest edit fields modal
                        await contestEditModalFieldsModalSubmit(interaction as ModalMessageModalSubmitInteraction, interactionObj.reference)
                        break;
                    case 4_00:
                        await helpThreadModalSubmit(interaction, interactionObj.reference)
                        break;
                }
            }
        }
        catch (e) {
            console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, e)
        }
    },
};