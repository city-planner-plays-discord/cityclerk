import {Events, ForumChannel, Message, ThreadChannel, ThreadMember} from "discord.js";
import {base} from "../main";
import {ConsoleColors} from "../classes/enums";
import prisma from "../client";

module.exports = {
    name: Events.ThreadCreate,
    async execute(thread: ThreadChannel, newlyCreated: boolean) {
        //console.info("THREAD CREATE")
        if (!newlyCreated) return 0 //if thread is old ignore

        else if (thread.parentId != null && await prisma.series.count({where: {seriesChannelID: thread.parentId}}) > 0) { //if thread new and a series channel send to suggestions airtable
            const [message, owner] = await Promise.all([thread.fetchStarterMessage(), thread.fetchOwner()]) as [Message<true> | null, ThreadMember | null]
            const parent = thread.parent || await thread.client.channels.fetch(thread.parentId) as ForumChannel
            if (message == null || owner == null || owner.user == null || parent == null) return console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, `message, owner, user, or parent is null`)

            try {
                const record = await base('Suggestions').create([
                    {
                        "fields": {
                            "Name": thread.name,
                            "Content": message.content,
                            "Series": parent.name,
                            "User": owner.user.tag,
                            "UserID": owner.id,
                            "Status": "To Do"
                        }
                    }
                ], {typecast: true});
                console.info(ConsoleColors.FGGREEN, `${new Date().toISOString()} .: Suggestion ${record[0]} by ${owner.user.tag} created`, ConsoleColors.RESET);
            }
            catch (e) {
                console.trace(`${ConsoleColors.FGRED}[ERROR]${ConsoleColors.RESET}`, e)
            }
        }
    },
};