import {
    AuditLogEvent,
    EmbedBuilder,
    Events, GuildMember,
    TextChannel,
    User
} from "discord.js";
import {Channels, EmbedColors} from "../classes/enums";

module.exports = {
    name: Events.GuildMemberRemove,
    async execute(member: GuildMember) {
        console.info("GUILD MEMBER REMOVE")
        const {entries: auditLogEntries} = await member.guild.fetchAuditLogs({type: AuditLogEvent.MemberKick, limit: 1})
        let logChannel, embed
        const kickAction = auditLogEntries.filter(({target}) => {
            if (target instanceof User)
                return target.id == member.id
        }).first()
        if (kickAction !== undefined && kickAction.target instanceof User) {
            embed = new EmbedBuilder()
                .setTitle(`[KICK] ${member.user.username}#${member.user.discriminator}`)
                .setColor(EmbedColors.DANGER)
                .addFields({
                        "name": `User`, "value": `<@${member.id}>`, "inline": true
                    },
                    {
                        "name": `Moderator`, "value": `<@${kickAction.target.id}>`, "inline": true
                    },
                    {
                        "name": `Reason`, "value": `${kickAction.reason || "none provided"}`, "inline": false
                    })
                .setThumbnail(member.displayAvatarURL())
                .setFooter({text: member.id})
            logChannel = await member.client.channels.fetch(Channels.MODERATION_LOG) as TextChannel
        }
        else {
            embed = new EmbedBuilder()
                .setTitle(`[LEFT] ${member.user.username}#${member.user.discriminator}`)
                .setColor(EmbedColors.INFO)
                .setDescription(`<@${member.id}> has left the server`)
                .setThumbnail(member.displayAvatarURL())
                .setFooter({text: member.id})
            logChannel = await member.client.channels.fetch(Channels.USER_LOG) as TextChannel
        }

        await logChannel.send({embeds: [embed]})
    },
};