import {
    AuditLogEvent,
    EmbedBuilder,
    Events,
    GuildAuditLogs,
    GuildBan,
    TextChannel,
    User
} from "discord.js";
import {Channels, EmbedColors} from "../classes/enums";

module.exports = {
    name: Events.GuildBanAdd,
    async execute({client, guild, user}: GuildBan) {
        console.info("GUILD BAN ADD")
        const [moderationLog, {entries: auditLogEntries}] = await Promise.all([client.channels.fetch(Channels.MODERATION_LOG), guild.fetchAuditLogs({type: AuditLogEvent.MemberBanAdd, limit: 1})]) as [TextChannel, GuildAuditLogs]
        const banAction = auditLogEntries.first()
        if (banAction !== undefined && banAction.target instanceof User && banAction.executor instanceof User) {
            const embed = new EmbedBuilder()
                .setTitle(`[BAN] ${user.username}#${user.discriminator}`)
                .setColor(EmbedColors.DANGER)
                .addFields({
                        "name": `User`, "value": `<@${user.id}>`, "inline": true
                    },
                    {
                        "name": `Moderator`, "value": `<@${banAction.executor.id}>`, "inline": true
                    },
                    {
                        "name": `Reason`, "value": `${banAction.reason || "none provided"}`, "inline": false
                    })
                .setThumbnail(user.displayAvatarURL())
                .setFooter({text: user.id})
            await moderationLog.send({embeds: [embed]})
        }
    },
};