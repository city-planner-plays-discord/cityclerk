import {ConsoleColors} from "../classes/enums";
import {Events} from "discord.js";

module.exports = {
    name: Events.ClientReady,
    once: true,
    execute(client: any) {
        console.log(ConsoleColors.FGGREEN, `Ready! Logged in as ${client.user.tag}`, ConsoleColors.RESET);
    },
};